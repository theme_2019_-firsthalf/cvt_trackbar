import cv2
import numpy as np
import os
import sys

h_low = 0
h_up = 255
s_low = 0
s_up = 255
v_low = 0
v_up = 255


def nothing(val):
    pass


def pram_show():
    cv2.namedWindow("threshold", cv2.WINDOW_NORMAL)
    cv2.createTrackbar("H_low", "threshold", h_low, 255, nothing)
    cv2.createTrackbar("H_up", "threshold", h_up, 255, nothing)
    cv2.createTrackbar("S_low", "threshold", s_low, 255, nothing)
    cv2.createTrackbar("S_up", "threshold", s_up, 255, nothing)
    cv2.createTrackbar("V_low", "threshold", v_low, 255, nothing)
    cv2.createTrackbar("V_up", "threshold", v_up, 255, nothing)


def read_parm():
    h_low = cv2.getTrackbarPos("H_low", "threshold")
    h_up = cv2.getTrackbarPos("H_up", "threshold")
    s_low = cv2.getTrackbarPos("S_low", "threshold")
    s_up = cv2.getTrackbarPos("S_up", "threshold")
    v_low = cv2.getTrackbarPos("V_low", "threshold")
    v_up = cv2.getTrackbarPos("V_up", "threshold")
    return h_low, h_up, s_low, s_up, v_low, v_up


def args_check(args):
    if len(args) == 2:
        filename = args[1]
    else:
        print(f'引数の数を確認してください\nargs = {args}')
        sys.exit(0)
    return filename


def input_file(filename):
    if not os.path.exists(filename):
        print('ファイルが存在しません')
        exit(0)
    # ファイルの読み込み
    img = cv2.imread(filename)
    print('in:' + filename)
    return img


def image_resize(img):
    org_width, org_height = img.shape[:2]
    size = (int(org_height/2), int(org_width/2))
    ret_img = cv2.resize(img, size)
    return ret_img


def image_cv(img, h_low, h_up, s_low, s_up, v_low, v_up):
    lower = np.array([h_low, s_low, v_low])
    upper = np.array([h_up, s_up, v_up])

    # imgの加工
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    binary = cv2.inRange(hsv_img, lower, upper)
    return binary


def preview(img, binary):
    cv2.imshow("image", img)
    cv2.imshow("binary", binary)


def dir_creater(filepath):
    if not os.path.exists(filepath):
        os.makedirs(filepath)


def save_img(filename, binary):
    filepath = 'result/'
    dir_creater(filepath)
    result_png_filename = filepath + 'result_' + filename
    cv2.imwrite(result_png_filename, binary)
    print('out:' + result_png_filename)


def save_pram_txt(filename, h_low, h_up, s_low, s_up, v_low, v_up):
    filepath = 'result/'
    dir_creater(filepath)
    result_txt_filename = filepath + filename[:-4] + '.txt'
    if os.path.exists(result_txt_filename):
        print("上書きします")
    else:
        print("新規作成します")

    with open(result_txt_filename, 'w') as f:
        f.write('h_low = ' + str(h_low) + '\n')
        f.write('h_up = ' + str(h_up) + '\n')
        f.write('s_low = ' + str(s_low) + '\n')
        f.write('s_up = ' + str(s_up) + '\n')
        f.write('v_low = ' + str(v_low) + '\n')
        f.write('v_up = ' + str(v_up) + '\n')
    print('out:' + result_txt_filename)


def save(filename, img, h_low, h_up, s_low, s_up, v_low, v_up):
    save_img(filename, img)
    save_pram_txt(filename, h_low, h_up, s_low, s_up, v_low, v_up)


if __name__ == "__main__":
    args = sys.argv
    filename = args_check(args)
    origin = input_file(filename)
    origin = image_resize(origin)

    pram_show()
    try:
        while True:
            h_low, h_up, s_low, s_up, v_low, v_up = read_parm()
            binary = image_cv(origin, h_low, h_up, s_low, s_up, v_low, v_up)
            preview(origin, binary)
            k = cv2.waitKey(1)  # Escで抜ける
            if k == 27:
                save(filename, binary, h_low, h_up, s_low, s_up, v_low, v_up)
                break
    except ValueError:
        save_img(filename, binary)
        cv2.destroyAllWindows()
        print("ValueError")
